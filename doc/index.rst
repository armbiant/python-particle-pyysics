.. PyPDT documentation master file, created by
   sphinx-quickstart on Wed Oct 29 21:06:27 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyPDT's documentation!
================================

.. toctree::
   :maxdepth: 2

This is the documentation for the PyPDT package for particle data access. This
documentation is based largely on the docstrings embedded in the Python code, so
the documentation you see here is the same as the documentation you can get by
calling `pydoc` or by using the `help()` function in the Python interpreter,
e.g.::

    $ python
    >>> import pypdt
    >>> help(pypdt)

Feel free to contact me at andy@insectnation.org if you find things I can
improve either in the documentation or the package itself.


Program
=======

PyPDT is mainly a Python programming package for accessing particle data
information, but also has a `pdt` command-line script for quick access to
particle summary data. This just accepts a list of particle ID codes and prints
out the particle summary information for each. The `-E` flag can be used to give
an energy in GeV and will print out the mean displacement that each requested
particle species would have at that energy (note that this is total energy, so
it must be at least the particle mass).::

    $ pdt 2212 211 511 -E 300
    p(P11): ID=2212, m=0.94 GeV, 3*q=3, width=0 GeV
    pi: ID=211, m=0.14 GeV, 3*q=3, width=2.5e-17 GeV, tau=2.6e+04 ps, ctau=7.8e+03 mm, mean disp=1.7e+07 mm
    B: ID=511, m=5.3 GeV, 3*q=0, width=4.3e-13 GeV, tau=1.5 ps, ctau=0.46 mm, mean disp=26 mm


Code docs
=========

.. toctree::
   :maxdepth: 2


Main PDT objects
----------------

The main types are the `ParticleDataTable` (aliased as `PDT` for convenience),
which is a database of many different particle species' vital statistics, and
`ParticleData` which is the container of that information for each particle.

Typical usage looks like this::

    import pypdt
    t = pypdt.PDT()
    print t[23].width
    print t[3122].mass
    print t[511].ctau

Both the HepPDT and PDG CSV data file formats can be read. By default the `PDT`
constructor loads the `mass_width_2006.csv` file from the Particle Data Group,
which is installed along with the module, but this can be overridden by passing
a constructor argument or by explicit calls to update the database from a
specified file.

.. automodule:: pypdt.pdt
   :members:


Particle ID functions
---------------------

PyPDT also contains functionality for particle property look-up from the PDG ID
code. Unbound functions similar to those in the HepPID C++ package are defined
in the `pypdt.pid` submodule and exported to the general `pypdt` namespace. They
are also (with the exception of `charge` and `threeCharge`) available via bound
properties on the `ParticleData` objects.

.. automodule:: pypdt.pid
   :members:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
